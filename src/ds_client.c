/*********************************************************************
	Client of DS-sim
	Department of Computing, Macquarie University, Australia
	Developed by Young Choon Lee since Jan. 2019
		
*********************************************************************/
#include <stdio.h>
#include <pwd.h>
#include <sys/socket.h> //For Sockets
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h> //For the AF_INET (Address Family)
#include <arpa/inet.h>
#include <unistd.h> 	// For fork
#include <assert.h>
#include <errno.h>
#include <ctype.h>
#include <signal.h>
#include <libxml/parser.h>

//#define RESC
#define VERSION					"12-May, 2021 @ MQ" // added FC and fixed scheduling issues with baseline algorithms
#define TRUE					1
#define FALSE					0
#define UNDEFINED				-1
#define UNKNOWN					UNDEFINED
#define MAX_BUF					1024
#define MAX_NAME_LENGTH			64
//#define RES_MSG_SIZE			(MAX_NAME_LENGTH + 3 + 10 + 7 + 3 + 7 + 7 + 10)
//#define RES_MSG_FIELDS			8
//#define RES_FAIL_MSG_SIZE		(10 + 7 + 7 + 7 + 7 + 7 + 10 + 10)
//#define RES_FAIL_MSG_FIELDS		8
#define DEFAULT_PORT			50000
#define DEFAULT_BASE			10
#define LOCAL_HOST_IP			"127.0.0.1"
#define SYS_INFO_FILENAME		"ds-system.xml"
#define END_DATA				"."

#define ROUND(x)				((int)((x) + 0.5))
#define intcomp(a, b)	((a) > (b) ? 1 : ((a) < (b) ? -1 : 0))

enum ResState {Inactive, Booting, Idle, Active, Unavailable};
enum GETS_Option {GO_All, GO_Type, GO_One, GO_Avail, GO_Capable, GO_Bounded, GO_END};	// GETS stands for GET Server info
enum JobState {Submitted, Waiting, Running, Suspended, Completed, Failed, Killed, END_JOB_STATE};
enum AlgoName {Algo_FF, Algo_BF, Algo_FAFFWJ, Algo_FAFFFC, Algo_FABFWJ, Algo_FABFFC, Algo_FC, Algo_WF, Algo_END};



typedef struct {
	int id;
	char *optstr;
	int optlen;
} GetsOption;

typedef struct {
	int cores;
	int mem;
	int disk;
} ResCapacity;

typedef struct {
	unsigned short type;
	char name[MAX_NAME_LENGTH];
	unsigned short limit;		// the max number of servers
	unsigned short bootupTime;
	ResCapacity capacity;
	float rate;					// hourly rate in cents; however, the actual charge is based on #seconds
} ServerTypeProp;

typedef struct {
	unsigned short type;
	int cores;
} ServType;

typedef struct {
	int numFails;
	int totalFailTime;
	int mttf;	// mean time to failure
	int mttr;	// mean time to recovery
	int madf;	// average absolute deviation or mean absolute deviation (MAD) for failures
	int madr;	// MAD for recoveries
	int lastOpTime;	// last time of operation whether it's the start time of server or the time after recovery (i.e., resume)
} ServerFailInfo;

typedef struct {
	//char stName[MAX_NAME_LENGTH];
	int type;
	int id;
	int startTime;
	int availCores;
	int availMem;		// in MB
	int availDisk;		// in MB
	int availTime;
	ServerFailInfo failInfo;
	int numWJobs;
	int numRJobs;
	enum ResState state;
} Server;

typedef struct {
	int state;
	char *stateStr;
} ServState;

typedef struct {
	unsigned short port;		// TCP/IP port number
	unsigned char numServTypes;	// the number of server types
	unsigned char maxServType;
	ServerTypeProp *sTypes;		// server properties per type
	Server **servers;
} System;

typedef struct {
	int id;
	int state;
	int submitTime;
	int startTime;
	int estRunTime;
	ResCapacity	resReq;
} Job;

typedef struct Schedule {
	int jID;
	int sType;
	int sID;
	int startTime;
	struct Schedule *next;
} Schedule;

typedef struct {
	int id;
	char name[MAX_NAME_LENGTH];
	Schedule *(*algoFunc)(Job *);
} Algorithm;

System systemInfo;
Schedule *head = NULL;
char *version = VERSION;
int algoID = UNKNOWN;
int verbose = FALSE;
int newline = FALSE;
int resFailure = FALSE;
int fd; //This is the socket file descriptor that will be used to identify the socket
ServType *sortedSTs = NULL;

const char *GetUserName();
int GetAlgoID(char *str);
int ConfigSim(int argc, char **argv);
void ShowUsage(char *progName);
inline void GracefulExit();
void PrintWelcomeMsg(int argc, char **argv);
void CompleteRecvMsg(char *msg, int length);
void CompleteSendMsg(char *msg, int length);
void SendMsg(int conn, char *msg);
void RecvMsg(int conn, char *msg, int msgSize);
int GetIntValue(char *str);
float GetFloatValue(char *str);
void StoreServerType(xmlNode *node);
int LoadSysInfo(xmlNode *node);
void InitServers(Server *servers, ServerTypeProp *sType);
void CreateServers();
int ReadSysInfo(char *filename);
void SortByServTypes();
void ResetServerStates();
int FindResTypeByName(char *name);
inline char *FindResTypeNameByType(int type);
int FindServStateID(char *stateStr);
int ReadResData(int fd, Job *job);
int RecvResData(int fd, Job *job, int nRecs, int recLen);
int IsSufficientAvailRes(ResCapacity *resReq, Server *server);
int IsSufficientResCapacity(ResCapacity *resReq, Server *server);
inline int GetServerBootupTime(int type);
int GetServerAvailTime(int sType, int sID, Job *job);
int CountJobs(int jState, int sType, int sID);
int GetEWT(int sType, int sID);
inline int EstimateTTF(ServerFailInfo *fInfo, int curTime);
int RecvLSTJData(int nRecs, int recLen);
int IsServerReady(Server *server, ResCapacity *jobResReq);

Schedule *ScheFirstFit(Job *job);
Schedule *ScheBestFit(Job *job);
Schedule *ScheFAFFWJ(Job *job);
Schedule *ScheFAFFFC(Job *job);
Schedule *ScheFABFWJ(Job *job);
Schedule *ScheFABFFC(Job *job);
Schedule *ScheFirstCapable(Job *job);
Schedule *ScheWorstFit(Job *job);
Schedule *ScheLargest(Job *job);

Schedule *PushSchd(Schedule *schd);
void FreeSchedules();
void FreeAll();

Algorithm algorithms[] = {
	{Algo_FF, "ff", ScheFirstFit},
	{Algo_BF, "bf", ScheBestFit},
	{Algo_FAFFWJ, "faffwj", ScheFAFFWJ},
	{Algo_FAFFFC, "fafffc", ScheFAFFFC},
	{Algo_FABFWJ, "fabfwj", ScheFABFWJ},
	{Algo_FABFFC, "fabffc", ScheFABFFC},
	{Algo_FC, "fc", ScheFirstCapable},
	{Algo_WF, "wf", ScheWorstFit}};

const GetsOption getsOptions[] = {{GO_All, "All", 3}, 
									{GO_Type, "Type", 4}, 
									{GO_One, "One", 3}, 
									{GO_Avail, "Avail", 5},
									{GO_Capable, "Capable", 7},
									{GO_Bounded, "Bounded", 7}};

ServState servStates[] = {{Inactive, "inactive"},
						{Booting, "booting"},
						{Idle, "idle"},
						{Active, "active"},
						{Unavailable, "unavailable"},
						{UNKNOWN, NULL}};

void SigHandler(int signo)
{
  if (signo == SIGINT)
	GracefulExit();
}

int main(int argc, char **argv)	// may use args: -p for port number, -u for username
{
	struct sockaddr_in serv; //This is our main socket variable.
	char nextCmd[MAX_BUF] = "", buffer[MAX_BUF] = ""; //This array will store the messages that are sent by the server
	int getsOpt = GO_Capable;
	const char *getsOptStr = getsOptions[getsOpt].optstr;
	// the following three arrays are for testing job migration (MIGJ)
	int *jIDs = (int*)calloc(10000, sizeof(int));
	int *sTs = (int*)calloc(10000, sizeof(int));
	int *sIDs = (int*)calloc(10000, sizeof(int));
	
	if (!ConfigSim(argc, argv)) {
		ShowUsage(argv[0]);
		return 1;
	}
	
	fd = socket(AF_INET, SOCK_STREAM, 0);
	serv.sin_family = AF_INET;
	serv.sin_port = htons(systemInfo.port);
	inet_pton(AF_INET, LOCAL_HOST_IP, &serv.sin_addr); 		//This binds the client to localhost
	if (connect(fd, (struct sockaddr *)&serv, sizeof(serv)) < 0) {	//This connects the client to the server.
		perror("connect"); 
		GracefulExit();
    }
	
	if (signal(SIGINT, SigHandler) == SIG_ERR)
		fprintf(stderr, "\ncan't catch SIGINT\n");
	
	PrintWelcomeMsg(argc, argv);

	strcpy(buffer, "HELO");
	SendMsg(fd, buffer);
	RecvMsg(fd, buffer, UNKNOWN);
	assert(!strcmp(buffer, "OK"));
	sprintf(buffer, "AUTH %s", GetUserName());
	SendMsg(fd, buffer);
	RecvMsg(fd, buffer, UNKNOWN);
	assert(!strcmp(buffer, "OK"));
	
	// read the system information file (system.xml)
	ReadSysInfo(SYS_INFO_FILENAME);
	// scheduling loop
	
	// this might not need anymore as ds-server sorts by default
		SortByServTypes();
	
	while (TRUE) {
		Job job = {0, 0, 0, 0, 0, {0, 0, 0}};
		
		if (!strlen(nextCmd))
			strcpy(buffer, "REDY");
		else
			strcpy(buffer, nextCmd);
		SendMsg(fd, buffer);
		nextCmd[0] = '\0';
		RecvMsg(fd, buffer, UNKNOWN);
		if (!strcmp(buffer, "NONE"))	// no more jobs to schedule
			break;
		else
		if (!strncmp(buffer, "JOB", 3)) {	// either "JOBN" for normal job submission or "JOBP" for resubmission of a pre-empted (failed/killed) job
			char cmd[MAX_BUF] = "";

			// cmd: 4 chars submit_time: int job_id: int estimated_runtime: int #cores_requested: int memory: int disk: int
			sscanf(buffer, "%s %d %d %d %d %d %d", cmd, &job.submitTime, &job.id, 
					&job.estRunTime, &job.resReq.cores, &job.resReq.mem, &job.resReq.disk);

			ResetServerStates();

#ifdef RESC
			sprintf(buffer, "RESC %s %d %d %d", getsOptStr, job.resReq.cores, job.resReq.mem, job.resReq.disk); 
#else
			sprintf(buffer, "GETS %s %d %d %d", getsOptStr, job.resReq.cores, job.resReq.mem, job.resReq.disk); 
#endif

			// either schedule directly (SCHD) or request resource information (GETS, formerly RESC); 
			// may use "KILJ" or "KILS" to kill a job or terminate a server, respectively
			while (TRUE) {
				int nRecs = 0;
				int recLen;

				SendMsg(fd, buffer);
				RecvMsg(fd, buffer, UNKNOWN);

				if (!strncmp(buffer, "DATA", 4)) {
					Schedule *schd;

#ifndef RESC
					sscanf(buffer, "DATA %d %d", &nRecs, &recLen);
#endif
					strcpy(buffer, "OK");

					SendMsg(fd, buffer);
#ifdef RESC
					ReadResData(fd, &job);
#else
					if (nRecs) {
						RecvResData(fd, &job, nRecs, recLen);
					}
					else { // '.' received when there are no available servers
						RecvMsg(fd, buffer, UNKNOWN);
						assert(!strcmp(buffer, END_DATA));
					}

#endif
					if (algoID == UNKNOWN)
						schd = ScheLargest(&job);
					else
						schd = algorithms[algoID].algoFunc(&job);

					// can't find an appropriate server to schedule the job
					if (schd->sType == UNKNOWN) {
						free(schd);
						if (!strcmp(getsOptStr, getsOptions[GO_Type].optstr) || 
							!strcmp(getsOptStr, getsOptions[GO_Avail].optstr)) {
							sprintf(buffer, "GETS %s", getsOptions[GO_All].optstr);
							continue;
						}
						strcpy(nextCmd, "PSHJ");
						break;
					}

					head = PushSchd(schd);

					sprintf(buffer, "SCHD %d %s %d", schd->jID, FindResTypeNameByType(schd->sType), schd->sID);

					jIDs[schd->jID] = TRUE;
					sTs[schd->jID] = schd->sType;
					sIDs[schd->jID] = schd->sID;
					
					SendMsg(fd, buffer);

					if (verbose) fprintf(stderr, "SELECTED server(%d): #%d of %s at %d for Job %d\n", 
						systemInfo.servers[schd->sType][schd->sID].state, schd->sID, 
						FindResTypeNameByType(schd->sType), schd->startTime, schd->jID);
					RecvMsg(fd, buffer, UNKNOWN);
					if (!strncmp(buffer, "ERR", 3)) {
						fprintf(stderr, "%s\n", buffer);
						goto Quit;
					}
					break;
				}
				else
				if (!strncmp(buffer, "ERR", 3)) {
					fprintf(stderr, "%s\n", buffer);
					goto Quit;
				}
			}
		}
		/*else
		if (!strncmp(buffer, "JCPL", 4)) {
			char cmd[MAX_BUF], sTypeName[MAX_NAME_LENGTH];
			int i, et, jID, sID, tgtSID;

			sscanf(buffer, "%s %d %d %s %d", cmd, &et, &jID, sTypeName, &sID);
			jIDs[jID] = FALSE;
			
			for (i = 10000; !jIDs[i-1] && i > 0; i--);
			tgtSID = (sIDs[i-1] < systemInfo.sTypes[FindResTypeByName(sTypeName)].limit - 1) ? sIDs[i-1] + 1 : 0;
			if (i > 0) {
				sprintf(buffer, "MIGJ %d %s %d %s %d", i-1, FindResTypeNameByType(sTs[i-1]), sIDs[i-1], FindResTypeNameByType(sTs[i-1]), tgtSID);
				SendMsg(fd, buffer);
				RecvMsg(fd, buffer, UNKNOWN);
				if (!strncmp(buffer, "OK", 2))
					sIDs[i-1] = tgtSID;
				else
					fprintf(stderr, "%s\n", buffer);
			}
		}*/
	}

Quit:
	strcpy(buffer, "QUIT");
	SendMsg(fd, buffer);
	RecvMsg(fd, buffer, UNKNOWN);
	assert(!strcmp(buffer, "QUIT"));
	close(fd);
	FreeSchedules();
	//if (algoID == Algo_FF)
		free(sortedSTs);
	
	return 0;
}

const char *GetUserName()
{
  uid_t uid = geteuid();
  struct passwd *pw = getpwuid(uid);
  if (pw)
  {
    return pw->pw_name;
  }

  return "";
}

int GetAlgoID(char *str)
{
	int i;
	
	for (i = 0; i < Algo_END && strcmp(algorithms[i].name, str); i++);
	
	if (i >= Algo_END) {
		fprintf(stderr, "Err: invalid algorithm (%s)!\n", str);
		return UNKNOWN;
	}
	
	return i;
}

int ConfigSim(int argc, char **argv)
{
	int c;
	
	systemInfo.port = DEFAULT_PORT;
	
	while ((c = getopt(argc, argv, "hvfa:p:n")) != -1) {
		switch (c)
		{
			case 'h':	// usage
				return FALSE;
			case 'v': 
				verbose = TRUE;
				break;
			case 'f':
				resFailure = TRUE;
				break;
			case 'a':	// algorithm
				algoID = GetAlgoID(optarg);
				if (algoID == UNKNOWN) return FALSE;
				break;
			case 'p':	// TCP/IP port
				systemInfo.port = GetIntValue(optarg);
				if (systemInfo.port == UNDEFINED) return FALSE;
				break;
			case 'n':	// new line ('\n') at the end of each message
				newline = TRUE;
				break;
			case '?':
				if (optopt == 'a' || optopt == 'p')
					fprintf(stderr, "Option -%c requires an argument.\n", optopt);
				else if (isprint(optopt))
					fprintf (stderr, "Unknown option `-%c'.\n", optopt);
				else
					fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
				return FALSE;
			default:
				break;
		}
	}

	return TRUE;
}

void ShowUsage(char *progName)
{
	printf("Usage:\n\t%s [-h] [-v] [-a algo_name]\n", progName);
}


inline void GracefulExit()
{
	close(fd);
	exit(EXIT_FAILURE);
}

void PrintWelcomeMsg(int argc, char **argv)
{
	int i;
	
	printf("# ds-sim reference client %s\n", version);
	printf("# Client-side simulator started with '");
	for (i = 0; i < argc; i++) {
		printf("%s", argv[i]);
		if (i + 1 < argc)
			printf(" ");
	}
	printf("'\n");
}

void CompleteRecvMsg(char *msg, int length)
{
	if (newline) {
		if (msg && msg[length-1] != '\n') {
			msg[length] = '\0';
			fprintf(stderr, "No new line character at the end of %s!\n", msg);
			GracefulExit();
		}
		length--;
	}
	msg[length] = '\0';
}

void CompleteSendMsg(char *msg, int length)
{
	if (newline) {
		msg[length] = '\n';
		msg[length+1] = '\0';
	}
}

void SendMsg(int conn, char *msg)
{
	char *orgMsg = strdup(msg);
//	char orgMsg[MAX_BUF];
	
	strcpy(orgMsg, msg);

	CompleteSendMsg(msg, strlen(msg));
	if ((send(conn, msg, strlen(msg), 0)) < 0) {
		fprintf(stderr, "%s wasn't not sent successfully!\n", msg);
		GracefulExit();
	}
	if (verbose)
		printf("C SENT %s\n", orgMsg);
	free(orgMsg);
}

void RecvMsg(int conn, char *msg, int msgSize)
{
	int recvCnt;
	
	if (msgSize <= 0)
		msgSize = MAX_BUF - 1;
	
	recvCnt = read(conn , msg, msgSize);
	CompleteRecvMsg(msg, recvCnt);
	if (verbose)
		printf("C RCVD %s\n", msg); 
}


int GetIntValue(char *str)
{
	int value;
	char *endptr;
	
	value = strtol(str, &endptr, DEFAULT_BASE);
	if (errno != 0) {
		perror("strtol");
		return UNDEFINED;
	}
	if (endptr == str) {
		fprintf(stderr, "No digits were found!\n");
		return UNDEFINED;
	}
	
	return value;
}

float GetFloatValue(char *str)
{
	float value;
	char *endptr;
	
	value = strtof(str, &endptr);

	if (errno != 0) {
		perror("strtof");
		return UNDEFINED;
	}
	if (endptr == str) {
		fprintf(stderr, "No digits were found!\n");
		return UNDEFINED;
	}

	return value;
}

// store server properties per type
void StoreServerType(xmlNode *node)
{
	ServerTypeProp *sTypes;
	int sID, nsTypes = systemInfo.numServTypes;
	ResCapacity *capacity;
	static int maxCoreCnt = 0;
	
	systemInfo.sTypes = (ServerTypeProp *)realloc(systemInfo.sTypes, sizeof(ServerTypeProp) * nsTypes);
	sTypes = systemInfo.sTypes;
	sID = nsTypes - 1;
	sTypes[sID].type = sID; 
	strcpy(sTypes[sID].name, (char *)xmlGetProp(node, (xmlChar *)"type"));
	sTypes[sID].limit = GetIntValue((char *)xmlGetProp(node, (xmlChar *)"limit"));
	sTypes[sID].bootupTime = GetIntValue((char *)xmlGetProp(node, (xmlChar *)"bootupTime"));
	sTypes[sID].rate = GetFloatValue((char *)xmlGetProp(node, (xmlChar *)"hourlyRate"));
	
	capacity = &sTypes[sID].capacity;
	
	capacity->cores = GetIntValue((char *)xmlGetProp(node, (xmlChar *)"coreCount"));
	capacity->mem = GetIntValue((char *)xmlGetProp(node, (xmlChar *)"memory"));	
	capacity->disk = GetIntValue((char *)xmlGetProp(node, (xmlChar *)"disk"));
	
	if (capacity->cores > maxCoreCnt) {
		maxCoreCnt = capacity->cores;
		systemInfo.maxServType = sID;
	}
	
}

int LoadSysInfo(xmlNode *node)
{
	xmlNode *curNode = node;
	int ret = TRUE;

	for (curNode = node; ret && curNode; curNode = curNode->next) {
		if (curNode->type == XML_ELEMENT_NODE) {
			if (!xmlStrcmp(curNode->name, (xmlChar *)"servers"))
				systemInfo.numServTypes = 0;
			else
			if (!xmlStrcmp(curNode->name, (xmlChar *)"server")) {
				systemInfo.numServTypes++;
				StoreServerType(curNode);
			}
		}
		ret = LoadSysInfo(curNode->children);
	}
	
	return ret;
}

void InitServers(Server *servers, ServerTypeProp *sType)
{
	int i, limit = sType->limit, type = sType->type;
	ResCapacity *capacity = &sType->capacity;
	
	for (i = 0; i < limit; i++) {
		servers[i].type = type;
		servers[i].id = i;
		servers[i].startTime = UNKNOWN;
		servers[i].availCores = capacity->cores;
		servers[i].availMem = capacity->mem;
		servers[i].availDisk = capacity->disk;
		servers[i].availTime = UNKNOWN;
		servers[i].failInfo.numFails = 0;
		servers[i].failInfo.totalFailTime = 0;
		servers[i].failInfo.mttf = UNKNOWN;
		servers[i].failInfo.mttr = UNKNOWN;
		servers[i].failInfo.madf = UNKNOWN;
		servers[i].failInfo.madr = UNKNOWN;
		servers[i].failInfo.lastOpTime = 0;
		servers[i].numWJobs = 0;
		servers[i].numRJobs = 0;
		//servers[i].state = Unavailable;
		servers[i].state = Inactive;
	}
}

void CreateServers()
{
	int nsTypes = systemInfo.numServTypes;
	ServerTypeProp *sTypes = systemInfo.sTypes;
	Server **servers = systemInfo.servers;
	int i;
	
	servers = (Server **)malloc(sizeof(Server *) * nsTypes);
	systemInfo.servers = servers;

	for (i = 0; i < nsTypes; i++) {
		ServerTypeProp *sType = &sTypes[i];
		int limit = sType->limit;
			
		servers[i] = (Server *)malloc(sizeof(Server) * limit);
		//InitServers(servers[i], sType->type, limit, capacity);
		InitServers(servers[i], sType);
	}
}

int ReadSysInfo(char *filename)
{
	xmlDoc *doc = NULL;
    xmlNode *curNode = NULL;
	int ret;

	doc = xmlReadFile(filename, NULL, 0);
	assert(doc);
	curNode = xmlDocGetRootElement(doc);
	
	ret = LoadSysInfo(curNode);
	
	if (ret)
		CreateServers();
	
	xmlFreeDoc(doc);
	xmlCleanupParser();
	
	return ret;
}

void ResetServerStates()
{
	int i, nsTypes = systemInfo.numServTypes;
	ServerTypeProp *sTypes = systemInfo.sTypes;
	Server **servers = systemInfo.servers;
	
	for (i = 0; i < nsTypes; i++) {
		ServerTypeProp *sType = &sTypes[i];
		int j, limit = sType->limit;
		
		for (j = 0; j < limit; j++)
			servers[i][j].state = Unavailable;
	}
}

int FindResTypeByName(char *name)
{
	ServerTypeProp *sTypes = systemInfo.sTypes;
	ServerTypeProp *sType;
	int i, nsTypes = systemInfo.numServTypes;

	for (i = 0, sType = &sTypes[i]; i < nsTypes && strcmp(sType->name, name); i++, sType = &sTypes[i]);
	assert (i < nsTypes);

	return sType->type;
}

inline char *FindResTypeNameByType(int type)
{
	ServerTypeProp *sTypes = systemInfo.sTypes;
	ServerTypeProp *sType;
	int i, nsTypes = systemInfo.numServTypes;

	for (i = 0, sType = &sTypes[i]; i < nsTypes && sType->type != type; i++, sType = &sTypes[i]);
	assert (i < nsTypes);
	
	return (systemInfo.sTypes[i].name);
}

// sort server types in ascending order by core count; this is currently used only for FF
void SortByServTypes()
{
	int i, j, numServTypes = systemInfo.numServTypes;
	ServerTypeProp *sTypes = systemInfo.sTypes;
	//qsort(systemInfo.sTypes, systemInfo.numServTypes, sizeof(ServerTypeProp), CompareCoreCnt);
	ServType *orgSTs = (ServType *)malloc(sizeof(ServType) * numServTypes);
	sortedSTs = (ServType *)malloc(sizeof(ServType) * numServTypes);
	
	assert(numServTypes);
	
	for (i = 0; i < numServTypes; i++) {
		orgSTs[i].type = sTypes[i].type;
		orgSTs[i].cores = sTypes[i].capacity.cores;
	}
	for (i = 0; i < numServTypes; i++) {
		ServType minST;
		minST.cores = INT_MAX;
		minST.type = UNKNOWN;
		for (j = 0; j < numServTypes; j++) {
			if (orgSTs[j].cores < minST.cores) {
				minST.type = orgSTs[j].type;
				minST.cores = orgSTs[j].cores;
			}
		}
		sortedSTs[i].type = minST.type;
		sortedSTs[i].cores = minST.cores;
		orgSTs[minST.type].cores = INT_MAX;
	}
		
	free(orgSTs);
}

int FindServStateID(char *stateStr)
{
	int i, state = UNKNOWN;
	
	for (i = 0; servStates[i].stateStr; i++)
		if (!strcmp(stateStr, servStates[i].stateStr))
			state = i;

	return state;
}


// DATA format: server_type: int server_id: int state: int avail_time: int avail_cores: int avail_mem: int avail_disk: int; 
// the available time will be -1 if there is a running task since the actual runtime is unknown
int ReadResData(int fd, Job *job)
{
	char buffer[MAX_BUF];
	int numServers = 0;
	Server **servers = systemInfo.servers;
	
	while (TRUE) {
		Server server;
		char stName[MAX_BUF], sStateStr[MAX_BUF];
		
		memset(buffer, 0, sizeof(buffer));
		RecvMsg(fd, buffer, UNKNOWN);
		if (!strcmp(buffer, END_DATA))
			break;
		
		server.numWJobs = server.numRJobs = 0;
		if (!resFailure) {
			sscanf(buffer, "%s %d %s %d %d %d %d",  stName, &server.id, sStateStr, 
				&server.startTime, &server.availCores, &server.availMem, &server.availDisk);
			server.type = FindResTypeByName(stName);
			servers[server.type][server.id].type = server.type;
			servers[server.type][server.id].id = server.id;
			servers[server.type][server.id].state = (enum ResState)FindServStateID(sStateStr);
			servers[server.type][server.id].startTime = server.startTime;
			servers[server.type][server.id].availCores = server.availCores;
			servers[server.type][server.id].availMem = server.availMem;
			servers[server.type][server.id].availDisk = server.availDisk;
		}
		else {
			sscanf(buffer, "%s %d %s %d %d %d %d %d %d %d %d %d %d",  
				stName, &server.id, sStateStr, 
				&server.startTime, &server.availCores, &server.availMem, &server.availDisk,
				&server.failInfo.numFails, &server.failInfo.totalFailTime, 
				&server.failInfo.mttf, &server.failInfo.mttr, 
				&server.failInfo.madf, &server.failInfo.lastOpTime);
				
			server.type = FindResTypeByName(stName);
			servers[server.type][server.id].type = server.type;
			servers[server.type][server.id].id = server.id;
			servers[server.type][server.id].state = (enum ResState)FindServStateID(sStateStr);
			servers[server.type][server.id].startTime = server.startTime;
			servers[server.type][server.id].availCores = server.availCores;
			servers[server.type][server.id].availMem = server.availMem;
			servers[server.type][server.id].availDisk = server.availDisk;
			servers[server.type][server.id].failInfo.numFails = server.failInfo.numFails;
			servers[server.type][server.id].failInfo.totalFailTime = server.failInfo.totalFailTime;
			servers[server.type][server.id].failInfo.mttf = server.failInfo.mttf;
			servers[server.type][server.id].failInfo.mttr = server.failInfo.mttr;
			servers[server.type][server.id].failInfo.madf = server.failInfo.madf;
			servers[server.type][server.id].failInfo.lastOpTime = server.failInfo.lastOpTime;
		}

		servers[server.type][server.id].availTime = GetServerAvailTime(server.type, server.id, job);
		
		numServers++;
		strcpy(buffer, "OK");
		SendMsg(fd, buffer);
	}
	
	return numServers;
}


int RecvResData(int fd, Job *job, int nRecs, int recLen)
{
	int numServers = 0;
	int msgSize = nRecs * recLen;
	Server **servers = systemInfo.servers, server;
	char *serverStates = (char *)malloc(sizeof(char) * msgSize + 1);
	char *buffer;
	char msgToSend[MAX_BUF], msgRcvd[MAX_BUF];


	RecvMsg(fd, serverStates, msgSize);
	buffer = serverStates;

	while (buffer) {
		char stName[MAX_BUF], sStateStr[MAX_BUF];

		if (!resFailure) {
			sscanf(buffer, "%s %d %s %d %d %d %d %d %d",  stName, &server.id, sStateStr, 
				&server.startTime, &server.availCores, &server.availMem, &server.availDisk, 
				&server.numWJobs, &server.numRJobs);

			server.type = FindResTypeByName(stName);
			servers[server.type][server.id].type = server.type;
			servers[server.type][server.id].id = server.id;
			servers[server.type][server.id].state = (enum ResState)FindServStateID(sStateStr);
			servers[server.type][server.id].startTime = server.startTime;
			servers[server.type][server.id].availCores = server.availCores;
			servers[server.type][server.id].availMem = server.availMem;
			servers[server.type][server.id].availDisk = server.availDisk;
			servers[server.type][server.id].numWJobs = server.numWJobs;
			servers[server.type][server.id].numRJobs = server.numRJobs;
		}
		else {
			sscanf(buffer, "%s %d %s %d %d %d %d %d %d %d %d %d %d %d %d",  
				stName, &server.id, sStateStr, 
				&server.startTime, &server.availCores, &server.availMem, &server.availDisk,
				&server.numWJobs, &server.numRJobs,
				&server.failInfo.numFails, &server.failInfo.totalFailTime, 
				&server.failInfo.mttf, &server.failInfo.mttr, 
				&server.failInfo.madf, &server.failInfo.lastOpTime);
				
			server.type = FindResTypeByName(stName);
			servers[server.type][server.id].type = server.type;
			servers[server.type][server.id].id = server.id;
			servers[server.type][server.id].state = (enum ResState)FindServStateID(sStateStr);
			servers[server.type][server.id].startTime = server.startTime;
			servers[server.type][server.id].availCores = server.availCores;
			servers[server.type][server.id].availMem = server.availMem;
			servers[server.type][server.id].availDisk = server.availDisk;
			servers[server.type][server.id].failInfo.numFails = server.failInfo.numFails;
			servers[server.type][server.id].failInfo.totalFailTime = server.failInfo.totalFailTime;
			servers[server.type][server.id].failInfo.mttf = server.failInfo.mttf;
			servers[server.type][server.id].failInfo.mttr = server.failInfo.mttr;
			servers[server.type][server.id].failInfo.madf = server.failInfo.madf;
			servers[server.type][server.id].failInfo.lastOpTime = server.failInfo.lastOpTime;
			servers[server.type][server.id].numWJobs = server.numWJobs;
			servers[server.type][server.id].numRJobs = server.numRJobs;
		}
		servers[server.type][server.id].availTime = GetServerAvailTime(server.type, server.id, job);

		if ((buffer = strchr(buffer, '\n')))
			buffer++;	// skip '\n' to read the next record
		numServers++;
	}

	strcpy(msgToSend, "OK");
	SendMsg(fd, msgToSend);
	RecvMsg(fd, msgRcvd, UNKNOWN);
	assert(!strcmp(msgRcvd, END_DATA));

	free(serverStates);

	return numServers;
}


int IsSufficientAvailRes(ResCapacity *resReq, Server *server)
{
	return (server->availCores >= resReq->cores &&
		server->availMem >= resReq->mem &&
		server->availDisk >= resReq->disk);
}

int IsSufficientResCapacity(ResCapacity *resReq, Server *server)
{
	ResCapacity *capacity = &systemInfo.sTypes[server->type].capacity;
	
	return (capacity->cores >= resReq->cores &&
				capacity->mem >= resReq->mem &&
				capacity->disk >= resReq->disk);
}


inline int GetServerBootupTime(int type)
{
	assert(type >= 0 && type <= systemInfo.numServTypes);
	return systemInfo.sTypes[type].bootupTime;
}


int GetServerAvailTime(int sType, int sID, Job *job)
{
	Server *server = &systemInfo.servers[sType][sID];
	ResCapacity *resReq = &job->resReq;
	int availTime = UNKNOWN;
	int curSimTime = job->submitTime;
	
	if (server->state == Idle)
		availTime = curSimTime;
	else
	if (server->state == Inactive)
		availTime = curSimTime + GetServerBootupTime(sType);
	
	if (resReq && availTime == UNKNOWN)	{// either Active or Booting
		if (IsSufficientAvailRes(resReq, server) && !server->numWJobs)
			availTime = (server->state == Booting) ? server->startTime : curSimTime;
	}
	
	return availTime;
}

int RecvLSTJData(int nRecs, int recLen)
{
	int ret = TRUE;
	int numJobs = 0;
	int msgSize = nRecs * recLen;
	Server **servers = systemInfo.servers, server;
	char *jobList = (char *)malloc(sizeof(char) * msgSize + 1);
	char *buffer;
	char msgToSend[MAX_BUF], msgRcvd[MAX_BUF];
	Job *jobs = (Job *)malloc(sizeof(Job) * nRecs);

	RecvMsg(fd, jobList, msgSize);
	buffer = jobList;

	while (buffer) {
		Job *job = &jobs[numJobs];
		char stName[MAX_BUF], sStateStr[MAX_BUF];

		sscanf(buffer, "%d %d %d %d %d %d %d %d", &job->id, &job->state, &job->submitTime, 
			&job->startTime, &job->estRunTime, &job->resReq.cores, &job->resReq.mem, &job->resReq.disk);
		if (job->startTime == UNKNOWN)
			ret = FALSE;
		if ((buffer = strchr(buffer, '\n')))
			buffer++;	// skip '\n' to read the next record
		numJobs++;
	}

	strcpy(msgToSend, "OK");
	SendMsg(fd, msgToSend);
	RecvMsg(fd, msgRcvd, UNKNOWN);
	assert(!strcmp(msgRcvd, END_DATA));

	free(jobList);
	free(jobs);

	return ret;
}

int IsServerReady(Server *server, ResCapacity *jobResReq)
{
	int ready = TRUE;
	
	if (server->state == Booting) {
		int nRecs = 0;
		int recLen;
		char buffer[MAX_BUF] = "";

		sprintf(buffer, "LSTJ %s %d", FindResTypeNameByType(server->type), server->id);
		SendMsg(fd, buffer);
		RecvMsg(fd, buffer, UNKNOWN);

		if (!strncmp(buffer, "DATA", 4)) {
			sscanf(buffer, "DATA %d %d", &nRecs, &recLen);
			strcpy(buffer, "OK");
			SendMsg(fd, buffer);

			if (nRecs) {
				ready = RecvLSTJData(nRecs, recLen);
			}
			else { // '.' received when there are no jobs
				RecvMsg(fd, buffer, UNKNOWN);
				assert(!strcmp(buffer, END_DATA));
			}
		}
	}
	else
	if (!server->numWJobs)
		ready = TRUE;
	
	return (ready && IsSufficientAvailRes(jobResReq, server));
}

Schedule *ScheFirstFit(Job *job)
{
	int i, nsTypes = systemInfo.numServTypes;
	int minAvailTime = UNKNOWN, minSType = UNKNOWN, minSID = UNKNOWN;
	int wstAvailTime = UNKNOWN, wstSType = UNKNOWN, wstSID = UNKNOWN;
	ServerTypeProp *sTypes = systemInfo.sTypes;
	Server **servers = systemInfo.servers;
	ResCapacity *resReq = &job->resReq;
	Schedule *newSchd = NULL;

	for (i = 0; i < nsTypes && minSType == UNKNOWN; i++) {
		ServerTypeProp *sType = &sTypes[sortedSTs[i].type];
		Server *server = servers[sType->type];
		int j, limit = sType->limit;

		for (j = 0; j < limit && minSType == UNKNOWN; j++) {
			if (server[j].state == Unavailable) continue;
#ifdef RESC
			server[j].numWJobs = CountJobs(Waiting, i, j);
#endif
			//if (!server[j].numWJobs && IsSufficientAvailRes(resReq, &server[j])) {
			if (IsServerReady(&server[j], resReq)) {
				minSType = sType->type;
				minSID = j;
				//minAvailTime = server[j].availTime;
				minAvailTime = GetServerAvailTime(i, j, job);
			}
			else // no idle/inactive server that can readily execute the job available
			if (wstSType == UNKNOWN && (server[j].state == Active || server[j].state == Booting) &&
				IsSufficientResCapacity(resReq, &server[j])) {
				wstSType = sType->type;
				wstSID = j;
				//wstAvailTime = server[j].availTime;
				wstAvailTime = GetServerAvailTime(i, j, job);
			}
		}
	}
	newSchd = (Schedule *)malloc(sizeof(Schedule));
	newSchd->jID = job->id;
	newSchd->sType = UNKNOWN;
	newSchd->sID = UNKNOWN;
	newSchd->startTime = UNKNOWN;
	newSchd->next = NULL;

	if (minSType != UNKNOWN) { // a server found for schedule the job
		newSchd->sType = minSType;
		newSchd->sID = minSID;
		newSchd->startTime = minAvailTime;
	}
	else
	if (wstSType != UNKNOWN)
	{
		newSchd->sType = wstSType;
		newSchd->sID = wstSID;
		newSchd->startTime = wstAvailTime;
	}

	return newSchd;
}


Schedule *ScheBestFit(Job *job)
{
	int i, nsTypes = systemInfo.numServTypes;
	int minAvailTime = UNKNOWN, minSType = UNKNOWN, minSID = UNKNOWN, minBF = INT_MAX;
	int wstAvailTime = UNKNOWN, wstSType = UNKNOWN, wstSID = UNKNOWN, wstBF = INT_MAX;
	ServerTypeProp *sTypes = systemInfo.sTypes;
	Server **servers = systemInfo.servers;
	ResCapacity *resReq = &job->resReq;
	Schedule *newSchd = NULL;
	
	for (i = 0; i < nsTypes; i++) {
		ServerTypeProp *sType = &sTypes[i];
		Server *server = servers[i];
		int j, limit = sType->limit;
		ResCapacity *capacity = &sType->capacity;
		
		for (j = 0; j < limit; j++) {
			if (server[j].state == Unavailable) continue;
			//if (!server[j].numWJobs && IsSufficientAvailRes(resReq, &server[j])) {
			if (IsServerReady(&server[j], resReq)) {
				int fit = server[j].availCores - resReq->cores;
				
				// Idle, Inactive or Active with sufficient resources for the job
				if (fit < minBF) {//  || (fit == minBF && server[j].availTime < minAvailTime)) {
					minSType = i;
					minSID = j;
					minAvailTime = server[j].availTime;
					minBF = fit;
				}
			}
			else // no idle/inactive server that can readily execute the job available
			if ((server[j].state == Active || server[j].state == Booting) &&
				IsSufficientResCapacity(resReq, &server[j]) &&
				capacity->cores - resReq->cores < wstBF) {
				wstSType = i;
				wstSID = j;
				wstBF = capacity->cores - resReq->cores;
			}
		}
	}
	
	newSchd = (Schedule *)malloc(sizeof(Schedule));
	newSchd->jID = job->id;
	newSchd->sType = UNKNOWN;
	newSchd->sID = UNKNOWN;
	newSchd->startTime = UNKNOWN;
	newSchd->next = NULL;
	
	if (minSType != UNKNOWN) { // a server found for schedule the job
		newSchd->sType = minSType;
		newSchd->sID = minSID;
		newSchd->startTime = minAvailTime;
	}
	else
	if (wstSType != UNKNOWN)
	{
		newSchd->sType = wstSType;
		newSchd->sID = wstSID;
		newSchd->startTime = wstAvailTime;
	}
	
	return newSchd;
}


inline int EstimateTTF(ServerFailInfo *fInfo, int curTime)
{
	int ttf =  fInfo->mttf - (curTime - fInfo->lastOpTime);
	
	return ttf;
}

// based primarily on #waiting jobs
Schedule *ScheFAFFWJ(Job *job)
{
	/*	0: immediately available with no failures so far, i.e. best case
		1: immediately available, but some failures before
		2: capable, not yet available with no failures so far
		3: capable, not yet available with some failures before, i.e., worst case
	*/
	enum SCHD_CANDIDATE {BEST_SCHD, BEST_F_SCHD, ALT_SCHD, ALT_F_SCHD, NUM_SCHD_CAND_TYPES};
	Schedule scheCandidates[NUM_SCHD_CAND_TYPES];	
	int i, nsTypes = systemInfo.numServTypes;
	ServerTypeProp *sTypes = systemInfo.sTypes;
	Server **servers = systemInfo.servers;
	ResCapacity *resReq = &job->resReq;
	Schedule *newSchd = NULL;
	int minWJCnt = INT_MAX;
	int minWJCntF = INT_MAX;
	int minFails = INT_MAX;
	
	for (i = 0; i < NUM_SCHD_CAND_TYPES; i++) {
		scheCandidates[i].startTime =
		scheCandidates[i].sType =
		scheCandidates[i].sID = UNKNOWN;
	}
	// scheCandidates[BEST_SCHD].sType == UNKNOWN --> stop searching as soon as the best candidate is found
	for (i = 0; i < nsTypes && scheCandidates[BEST_SCHD].sType == UNKNOWN; i++) {
		ServerTypeProp *sType = &sTypes[sortedSTs[i].type];
		Server *server = servers[sType->type];
		int j, limit = sType->limit;

		for (j = 0; j < limit && scheCandidates[BEST_SCHD].sType == UNKNOWN; j++) {
			Server *sInstance = &server[j];
			ServerFailInfo *fInfo = &sInstance->failInfo;
			int wJobCnt, suffRes, schdCand;
			
			if (sInstance->state == Unavailable) continue;
			
			schdCand = UNKNOWN; 
			wJobCnt = sInstance->numWJobs;
			suffRes = IsSufficientAvailRes(resReq, sInstance);
						
			if (!fInfo->numFails && !wJobCnt && suffRes)
				schdCand = BEST_SCHD;
			else
			if (fInfo->numFails && !wJobCnt && suffRes && fInfo->numFails < minFails) {
					schdCand = BEST_F_SCHD;
					minFails = fInfo->numFails;
			}
			else // no idle/inactive server that can readily execute the job available
			if ((sInstance->state == Active || sInstance->state == Booting) && IsSufficientResCapacity(resReq, sInstance)) {
				if (!fInfo->numFails && wJobCnt < minWJCnt) { // may consider the total estRunTime of waiting jobs
					schdCand = ALT_SCHD;
					minWJCnt = wJobCnt;
				}
				else
				if (fInfo->numFails && wJobCnt < minWJCntF) {
					schdCand = ALT_F_SCHD;
					minWJCntF = wJobCnt;
				}
			}
			if (schdCand != UNKNOWN) {
				scheCandidates[schdCand].sType = sType->type;
				scheCandidates[schdCand].sID = j;
				scheCandidates[schdCand].startTime = sInstance->availTime;
			}
		}
	}
	newSchd = (Schedule *)malloc(sizeof(Schedule));
	newSchd->jID = job->id;
	newSchd->sType = UNKNOWN;
	newSchd->sID = UNKNOWN;
	newSchd->startTime = UNKNOWN;
	newSchd->next = NULL;

	for (i = 0; i < NUM_SCHD_CAND_TYPES; i++)
		if (scheCandidates[i].sType != UNKNOWN) {
			newSchd->sType = scheCandidates[i].sType;
			newSchd->sID = scheCandidates[i].sID;
			newSchd->startTime = scheCandidates[i].startTime;
			break;
		}

	return newSchd;
}


Schedule *ScheFAFFFC(Job *job)
{
	enum SCHD_CANDIDATE {BEST_SCHD, BEST_F_SCHD, ALT_SCHD, ALT_F_SCHD, NUM_SCHD_CAND_TYPES};
	Schedule scheCandidates[NUM_SCHD_CAND_TYPES];	
	int i, nsTypes = systemInfo.numServTypes;
	ServerTypeProp *sTypes = systemInfo.sTypes;
	Server **servers = systemInfo.servers;
	ResCapacity *resReq = &job->resReq;
	Schedule *newSchd = NULL;
	int minWJCnt = INT_MAX;
	int minWJCntF = INT_MAX;
	int minFails = INT_MAX;
	int minFailsF = INT_MAX;
	int totalWJCnt = 0, numCandiServers = 0;
	int avgWJCnt;
	
	for (i = 0; i < NUM_SCHD_CAND_TYPES; i++) {
		scheCandidates[i].startTime =
		scheCandidates[i].sType =
		scheCandidates[i].sID = UNKNOWN;
	}
	// scheCandidates[BEST_SCHD].sType == UNKNOWN --> stop searching as soon as the best candidate is found
	for (i = 0; i < nsTypes && scheCandidates[BEST_SCHD].sType == UNKNOWN; i++) {
		ServerTypeProp *sType = &sTypes[sortedSTs[i].type];
		Server *server = servers[sType->type];
		int j, limit = sType->limit;

		for (j = 0; j < limit && scheCandidates[BEST_SCHD].sType == UNKNOWN; j++) {
			Server *sInstance = &server[j];
			ServerFailInfo *fInfo = &sInstance->failInfo;
			int wJobCnt, suffRes, schdCand;
			
			if (sInstance->state == Unavailable) continue;
			
			schdCand = UNKNOWN; 
			wJobCnt = sInstance->numWJobs;;
			suffRes = IsSufficientAvailRes(resReq, sInstance);

			if (IsSufficientResCapacity(resReq, sInstance) && fInfo->numFails) {
				totalWJCnt += wJobCnt;
				numCandiServers++;
			}

			if (!fInfo->numFails && !wJobCnt && suffRes)
				schdCand = BEST_SCHD;
			else
			if (fInfo->numFails && !wJobCnt && suffRes && fInfo->numFails < minFails) {
					schdCand = BEST_F_SCHD;
					minFails = fInfo->numFails;
			}
			else // no idle/inactive server that can readily execute the job available
			if ((sInstance->state == Active || sInstance->state == Booting) && IsSufficientResCapacity(resReq, sInstance)) {
				if (!fInfo->numFails && wJobCnt < minWJCnt) { // may consider the total estRunTime of waiting jobs
					schdCand = ALT_SCHD;
					minWJCnt = wJobCnt;
				}
				else
				if (fInfo->numFails && (fInfo->numFails < minFailsF || (fInfo->numFails == minFailsF && wJobCnt < minWJCntF ))) {
					schdCand = ALT_F_SCHD;
					minWJCntF = wJobCnt;
					minFailsF = fInfo->numFails;
				}
			}
			if (schdCand != UNKNOWN) {
				scheCandidates[schdCand].sType = sType->type;
				scheCandidates[schdCand].sID = j;
				scheCandidates[schdCand].startTime = sInstance->availTime;
			}
		}
	}
	newSchd = (Schedule *)malloc(sizeof(Schedule));
	newSchd->jID = job->id;
	newSchd->sType = UNKNOWN;
	newSchd->sID = UNKNOWN;
	newSchd->startTime = UNKNOWN;
	newSchd->next = NULL;

	for (i = 0; i < NUM_SCHD_CAND_TYPES; i++)
		if (scheCandidates[i].sType != UNKNOWN) {
			newSchd->sType = scheCandidates[i].sType;
			newSchd->sID = scheCandidates[i].sID;
			newSchd->startTime = scheCandidates[i].startTime;
			break;
		}

	avgWJCnt = numCandiServers ? ROUND((float)totalWJCnt / numCandiServers) : 0;
	// too many waiting jobs (twice as many) on ALT_SCHD server, then use ALT_F_SCHD server
	if (i == ALT_SCHD && scheCandidates[ALT_F_SCHD].sType != UNKNOWN &&	
		minWJCnt > avgWJCnt && minWJCnt >= minWJCntF * 3) {
		newSchd->sType = scheCandidates[ALT_F_SCHD].sType;
		newSchd->sID = scheCandidates[ALT_F_SCHD].sID;
		newSchd->startTime = scheCandidates[ALT_F_SCHD].startTime;
	}

	return newSchd;
}


// based primarily on #waiting jobs
Schedule *ScheFABFWJ(Job *job)
{
	/*	0: immediately available with no failures so far, i.e. best case
		1: immediately available, but some failures before
		2: capable, not yet available with no failures so far
		3: capable, not yet available with some failures before, i.e., worst case
	*/
	enum SCHD_CANDIDATE {BEST_SCHD, BEST_F_SCHD, ALT_SCHD, ALT_F_SCHD, NUM_SCHD_CAND_TYPES};
	Schedule scheCandidates[NUM_SCHD_CAND_TYPES];	
	int i, nsTypes = systemInfo.numServTypes;
	ServerTypeProp *sTypes = systemInfo.sTypes;
	Server **servers = systemInfo.servers;
	ResCapacity *resReq = &job->resReq;
	Schedule *newSchd = NULL;
	int minWJCnt = INT_MAX;
	int minWJCntF = INT_MAX;
	int minFit = INT_MAX;
	int minFitF = INT_MAX;
	int minAvailTime = INT_MAX, minAvailTimeF = INT_MAX;
	
	for (i = 0; i < NUM_SCHD_CAND_TYPES; i++) {
		scheCandidates[i].startTime =
		scheCandidates[i].sType =
		scheCandidates[i].sID = UNKNOWN;
	}
	// scheCandidates[BEST_SCHD].sType == UNKNOWN --> stop searching as soon as the best candidate is found
	for (i = 0; i < nsTypes && scheCandidates[BEST_SCHD].sType == UNKNOWN; i++) {
		ServerTypeProp *sType = &sTypes[sortedSTs[i].type];
		Server *server = servers[sType->type];
		int j, limit = sType->limit;

		for (j = 0; j < limit && scheCandidates[BEST_SCHD].sType == UNKNOWN; j++) {
			Server *sInstance = &server[j];
			ServerFailInfo *fInfo = &sInstance->failInfo;
			int wJobCnt, suffRes, fit, schdCand, availTime;
			
			if (sInstance->state == Unavailable) continue;
			
			schdCand = UNKNOWN;
			wJobCnt = sInstance->numWJobs;
			suffRes = IsSufficientAvailRes(resReq, sInstance);
			availTime = sInstance->availTime;
			
			if (suffRes && !wJobCnt) {
				fit = sInstance->availCores - resReq->cores;
			
				if (!fInfo->numFails) {
					if (availTime < minAvailTime || (availTime == minAvailTime && fit < minFit)) {
						schdCand = BEST_SCHD;
						minFit = fit;
						minAvailTime = availTime;
					}
				}
				else
				if (availTime < minAvailTimeF || (availTime == minAvailTimeF && fit < minFitF)) {
						schdCand = BEST_F_SCHD;
						minFitF = fit;
						minAvailTimeF = availTime;
				}
			}
			else // no idle/inactive server that can readily execute the job available
			if ((sInstance->state == Active || sInstance->state == Booting) && IsSufficientResCapacity(resReq, sInstance)) {
				if (!fInfo->numFails && wJobCnt < minWJCnt) { // may consider the total estRunTime of waiting jobs
					schdCand = ALT_SCHD;
					minWJCnt = wJobCnt;
				}
				else
				if (fInfo->numFails && wJobCnt < minWJCntF) {
					schdCand = ALT_F_SCHD;
					minWJCntF = wJobCnt;
				}
			}
			if (schdCand != UNKNOWN) {
				scheCandidates[schdCand].sType = sType->type;
				scheCandidates[schdCand].sID = j;
				scheCandidates[schdCand].startTime = availTime;
			}
		}
	}
	newSchd = (Schedule *)malloc(sizeof(Schedule));
	newSchd->jID = job->id;
	newSchd->sType = UNKNOWN;
	newSchd->sID = UNKNOWN;
	newSchd->startTime = UNKNOWN;
	newSchd->next = NULL;

	for (i = 0; i < NUM_SCHD_CAND_TYPES; i++)
		if (scheCandidates[i].sType != UNKNOWN) {
			newSchd->sType = scheCandidates[i].sType;
			newSchd->sID = scheCandidates[i].sID;
			newSchd->startTime = scheCandidates[i].startTime;
			break;
		}

	return newSchd;
}

// based primarily on #waiting jobs
Schedule *ScheFABFFC(Job *job)
{
	/*	0: immediately available with no failures so far, i.e. best case
		1: immediately available, but some failures before
		2: capable, not yet available with no failures so far
		3: capable, not yet available with some failures before, i.e., worst case
	*/
	enum SCHD_CANDIDATE {BEST_SCHD, BEST_F_SCHD, ALT_SCHD, ALT_F_SCHD, NUM_SCHD_CAND_TYPES};
	Schedule scheCandidates[NUM_SCHD_CAND_TYPES];	
	int i, nsTypes = systemInfo.numServTypes;
	ServerTypeProp *sTypes = systemInfo.sTypes;
	Server **servers = systemInfo.servers;
	ResCapacity *resReq = &job->resReq;
	Schedule *newSchd = NULL;
	int minWJCnt = INT_MAX;
	int minWJCntF = INT_MAX;
	int minFit = INT_MAX;
	int minFitF = INT_MAX;
	int minAvailTime = INT_MAX, minAvailTimeF = INT_MAX;
	int minFailsF = INT_MAX;
	int totalWJCnt = 0, numCandiServers = 0;
	int avgWJCnt;
	
	for (i = 0; i < NUM_SCHD_CAND_TYPES; i++) {
		scheCandidates[i].startTime =
		scheCandidates[i].sType =
		scheCandidates[i].sID = UNKNOWN;
	}
	// scheCandidates[BEST_SCHD].sType == UNKNOWN --> stop searching as soon as the best candidate is found
	for (i = 0; i < nsTypes && scheCandidates[BEST_SCHD].sType == UNKNOWN; i++) {
		ServerTypeProp *sType = &sTypes[sortedSTs[i].type];
		Server *server = servers[sType->type];
		int j, limit = sType->limit;

		for (j = 0; j < limit && scheCandidates[BEST_SCHD].sType == UNKNOWN; j++) {
			Server *sInstance = &server[j];
			ServerFailInfo *fInfo = &sInstance->failInfo;
			int wJobCnt, suffRes, fit, schdCand, availTime;
			
			if (sInstance->state == Unavailable) continue;
			
			schdCand = UNKNOWN;
			wJobCnt = sInstance->numWJobs;
			suffRes = IsSufficientAvailRes(resReq, sInstance);
			availTime = sInstance->availTime;
			
			if (IsSufficientResCapacity(resReq, sInstance) && fInfo->numFails) {
				totalWJCnt += wJobCnt;
				numCandiServers++;
			}
			
			if (suffRes && !wJobCnt) {
				fit = sInstance->availCores - resReq->cores;
			
				if (!fInfo->numFails) {
					if (availTime < minAvailTime || (availTime == minAvailTime && fit < minFit)) {
						schdCand = BEST_SCHD;
						minFit = fit;
						minAvailTime = availTime;
					}
				}
				else
				if (availTime < minAvailTimeF || (availTime == minAvailTimeF && fit < minFitF)) {
						schdCand = BEST_F_SCHD;
						minFitF = fit;
						minAvailTimeF = availTime;
				}
			}
			else // no idle/inactive server that can readily execute the job available
			if ((sInstance->state == Active || sInstance->state == Booting) && IsSufficientResCapacity(resReq, sInstance)) {
				if (!fInfo->numFails && wJobCnt < minWJCnt) { // may consider the total estRunTime of waiting jobs
					schdCand = ALT_SCHD;
					minWJCnt = wJobCnt;
				}
				else
				if (fInfo->numFails && (fInfo->numFails < minFailsF || (fInfo->numFails == minFailsF && wJobCnt < minWJCntF ))) {
					schdCand = ALT_F_SCHD;
					minWJCntF = wJobCnt;
					minFailsF = fInfo->numFails;
				}
			}
			if (schdCand != UNKNOWN) {
				scheCandidates[schdCand].sType = sType->type;
				scheCandidates[schdCand].sID = j;
				scheCandidates[schdCand].startTime = availTime;
			}
		}
	}
	newSchd = (Schedule *)malloc(sizeof(Schedule));
	newSchd->jID = job->id;
	newSchd->sType = UNKNOWN;
	newSchd->sID = UNKNOWN;
	newSchd->startTime = UNKNOWN;
	newSchd->next = NULL;

	for (i = 0; i < NUM_SCHD_CAND_TYPES; i++)
		if (scheCandidates[i].sType != UNKNOWN) {
			newSchd->sType = scheCandidates[i].sType;
			newSchd->sID = scheCandidates[i].sID;
			newSchd->startTime = scheCandidates[i].startTime;
			break;
		}

	avgWJCnt = numCandiServers ? ROUND((float)totalWJCnt / numCandiServers) : 0;
	// too many waiting jobs (twice as many) on ALT_SCHD server, then use ALT_F_SCHD server
	if (i == ALT_SCHD && scheCandidates[ALT_F_SCHD].sType != UNKNOWN &&	
		minWJCnt > avgWJCnt && minWJCnt >= minWJCntF * 3) {
		newSchd->sType = scheCandidates[ALT_F_SCHD].sType;
		newSchd->sID = scheCandidates[ALT_F_SCHD].sID;
		newSchd->startTime = scheCandidates[ALT_F_SCHD].startTime;
	}
	
	return newSchd;
}

Schedule *ScheFirstCapable(Job *job)
{
	int i, nsTypes = systemInfo.numServTypes;
	int fcSType = UNKNOWN, fcSID = UNKNOWN;
	ServerTypeProp *sTypes = systemInfo.sTypes;
	Server **servers = systemInfo.servers;
	ResCapacity *resReq = &job->resReq;
	Schedule *newSchd = NULL;

	for (i = 0; i < nsTypes && fcSType == UNKNOWN; i++) {
		ServerTypeProp *sType = &sTypes[sortedSTs[i].type];
		Server *server = servers[sType->type];
		int j, limit = sType->limit;

		for (j = 0; j < limit && fcSType == UNKNOWN; j++) {
			if (server[j].state == Unavailable) continue;
			if (IsSufficientResCapacity(resReq, &server[j])) {
				fcSType = sType->type;
				fcSID = j;
			}
		}
	}

	assert(fcSType != UNKNOWN);
	
	newSchd = (Schedule *)malloc(sizeof(Schedule));
	newSchd->jID = job->id;
	newSchd->sType = fcSType;
	newSchd->sID = fcSID;
	newSchd->startTime = UNKNOWN;
	newSchd->next = NULL;

	return newSchd;
}

Schedule *ScheWorstFit(Job *job)
{
	int i, nsTypes = systemInfo.numServTypes;
	int maxAvailTime = UNKNOWN, maxSType = UNKNOWN, maxSID = UNKNOWN, maxWF = INT_MIN;
	int wstAvailTime = UNKNOWN, wstSType = UNKNOWN, wstSID = UNKNOWN, wstWF = INT_MIN;
	ServerTypeProp *sTypes = systemInfo.sTypes;
	Server **servers = systemInfo.servers;
	ResCapacity *resReq = &job->resReq;
	Schedule *newSchd = NULL;
	
	for (i = 0; i < nsTypes; i++) {
		ServerTypeProp *sType = &sTypes[i];
		Server *server = servers[i];
		int j, limit = sType->limit;
		ResCapacity *capacity = &sType->capacity;
		
		for (j = 0; j < limit; j++) {
			if (server[j].state == Unavailable) continue;

			//if (!server[j].numWJobs && IsSufficientAvailRes(resReq, &server[j])) {
			if (IsServerReady(&server[j], resReq)) {
				int fit = server[j].availCores - resReq->cores;

				if (fit > maxWF) {
					maxSType = i;
					maxSID = j;
					maxAvailTime = server[j].availTime;
					maxWF = fit;
				}
			}
			else // no idle/inactive server that can readily execute the job available
			if ((server[j].state == Active || server[j].state == Booting) &&
				IsSufficientResCapacity(resReq, &server[j]) &&
				capacity->cores - resReq->cores > wstWF) {
				wstSType = i;
				wstSID = j;
				wstWF = capacity->cores - resReq->cores;
				wstAvailTime = server[j].availTime;
			}
		}
	}
	newSchd = (Schedule *)malloc(sizeof(Schedule));
	newSchd->jID = job->id;
	newSchd->sType = UNKNOWN;
	newSchd->sID = UNKNOWN;
	newSchd->startTime = UNKNOWN;
	newSchd->next = NULL;
	
	if (maxSType != UNKNOWN) { // a server found for schedule the job
		newSchd->sType = maxSType;
		newSchd->sID = maxSID;
		newSchd->startTime = maxAvailTime;
	}
	else
	if (wstSType != UNKNOWN)
	{
		newSchd->sType = wstSType;
		newSchd->sID = wstSID;
		newSchd->startTime = wstAvailTime;
	}

	return newSchd;
}

Schedule *ScheLargest(Job *job)
{
	Schedule *newSchd = (Schedule *)malloc(sizeof(Schedule));
	newSchd->jID = job->id;
	newSchd->next = NULL;
	newSchd->sType = systemInfo.maxServType;
	newSchd->sID = 0;
	newSchd->startTime = systemInfo.servers[newSchd->sType][newSchd->sID].state != Unavailable ? 
					systemInfo.servers[newSchd->sType][newSchd->sID].availTime : UNKNOWN;	
					
	return newSchd;
}

int CountJobs(int jState, int sType, int sID)
{
	char buffer[MAX_BUF];
	int cnt;

	sprintf(buffer, "CNTJ %s %d %d", FindResTypeNameByType(sType), sID, jState);
	SendMsg(fd, buffer);
	
	RecvMsg(fd, buffer, UNKNOWN);
	sscanf(buffer, "%d", &cnt);
	
	return cnt;
}


int GetEWT(int sType, int sID)
{
	char buffer[MAX_BUF];
	int wt;

	sprintf(buffer, "EJWT %s %d", FindResTypeNameByType(sType), sID);
	SendMsg(fd, buffer);
	
	RecvMsg(fd, buffer, UNKNOWN);
	sscanf(buffer, "%d", &wt);
	
	return wt;
}


Schedule *PushSchd(Schedule *schd)
{
	schd->next = head;
	return schd;
}


void FreeSchedules()
{
	Schedule *cur, *next;
	
	for (cur = head; cur; cur = next)
	{
		next = cur->next;
		free(cur);
	}
}

void FreeSystemInfo()
{
	int i, nsTypes = systemInfo.numServTypes;
	ServerTypeProp *sTypes = systemInfo.sTypes;
	Server **servers = systemInfo.servers;

	if (!sTypes) return;	// nothing to free
	if (servers) {
		for (i = 0; i < nsTypes; i++)
			free(servers[i]);
		free(servers);
		systemInfo.servers = NULL;
	}

	free(sTypes);
	systemInfo.sTypes = NULL;
}

void FreeAll()
{
	FreeSystemInfo();
	FreeSchedules();
}
